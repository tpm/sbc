# public headers
install_headers('sbc.h', subdir: 'sbc')

# library
sbc_sources = [
  'sbc.c',
  'sbc_primitives.c',
  'sbc_primitives_mmx.c',
  'sbc_primitives_iwmmxt.c',
  'sbc_primitives_neon.c',
  'sbc_primitives_armv6.c',
  'sbc_primitives_sse.c',
]

sbc_soversion_major = sbc_current - sbc_age
sbc_soversion_minor = sbc_age
sbc_soversion_patch = sbc_revision

soversion = sbc_soversion_major
libversion = '@0@.@1@.@2@'.format(soversion, sbc_soversion_minor, sbc_soversion_patch)

sbc_ldflags = []

mapfile = 'sbc.sym'
sym_ldflag = '-Wl,--version-script,@0@/@1@'.format(meson.current_source_dir(), mapfile)
if cc.has_link_argument(sym_ldflag)
  sbc_ldflags += [sym_ldflag]
endif

libsbc = library('sbc', sbc_sources,
  include_directories: config_inc,
  soversion: soversion,
  version: libversion,
  link_args : sbc_ldflags,
  link_depends: mapfile,
  vs_module_defs: 'sbc.def',
  install: true)

sbc_dep = declare_dependency(link_with: libsbc)

# pkg-config file
pkg = import('pkgconfig')
pkg.generate(libsbc,
  name: 'sbc',
  description: 'SBC library',
  version: meson.project_version())
